from django import forms
from lab_1.models import Friend
class FriendForm(forms.ModelForm) :
    class Meta :
        model = Friend;
        fields = ['name','npm','DOB']
    
    input_attrs = {
        'name' : {
            'type' : 'text',
            'placeholder' : 'Masukkan nama'
        },
        'npm' : {
            'type' : 'text',
            'placeholder' : 'Masukkan npm'
        },
        'DOB' : {
            'type' :'date',
        }
        
    }
    
    name = forms.CharField(required=True, max_length=30, widget=forms.TextInput(attrs = input_attrs['name']))
    npm = forms.CharField(required= True, max_length=10, widget=forms.TextInput(attrs = input_attrs['npm']))
    DOB = forms.DateField(required=True, widget=forms.DateInput(attrs = input_attrs['DOB']))
        
