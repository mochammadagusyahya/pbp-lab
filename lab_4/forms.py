from django import forms
from lab_2.models import Note

class NoteForm(forms.ModelForm):
    class Meta:
        model = Note;
        fields = ['to','From','title','message']
    input_attrs = {
        'to' : {
            'type' : 'text',
            'placeholder': 'To'
        },
        'From' : {
            'type' : 'text',
            'placeholder': 'From'
        },
        'title': {
            'type' : 'text',
            'placeholder': 'Title'
        },
        'message': {
            'rows':'5',
            'cols' : '50',
            'placeholder': 'Message..'
        }
    }
    to = forms.CharField(required=True, max_length=30, widget=forms.TextInput(attrs = input_attrs['to']))
    From = forms.CharField(required=True, max_length=30, widget=forms.TextInput(attrs = input_attrs['From']))
    title = forms.CharField(required=True, max_length=30, widget=forms.TextInput(attrs = input_attrs['title']))
    message = forms.CharField(required=True, max_length=100, widget=forms.Textarea(attrs = input_attrs['message']))