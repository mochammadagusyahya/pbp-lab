//@dart=2.9
import 'package:flutter/material.dart';

void main() {
  runApp(MaterialApp(
    title: "Form Flutter",
    home: BelajarForm(),
  ));
}

class BelajarForm extends StatefulWidget {
  @override
  _BelajarFormState createState() => _BelajarFormState();
}

class _BelajarFormState extends State<BelajarForm> {
  void createAlertDialog(String name) {
    showDialog(context: context, builder: (BuildContext context){
      return AlertDialog(
        title: Text("Helloo " + name + " !"),
        content: Text("Welcome to Flutter.com !"),
        actions: <Widget>[
          FlatButton(onPressed: Navigator.of(context).pop, child: Text("Close")),
        ],
      );
    });
  }
  final _formKey = GlobalKey<FormState>();
  
  String name = "";
  double nilaiSlider = 1;
  bool nilaiCheckBox = false;
  bool nilaiCheckBox1 = false;
  bool nilaiCheckBox2 = false;
  bool nilaiSwitch = false;
  switchBackground(bool nilaiSwitch) {
    if (nilaiSwitch == true) {
      return Color(0xBBBBBBBff);
    }
  }

  int radioValue = 0;
  void radioValueChange(int value) {
    setState(() {
      radioValue = value;

      switch (radioValue) {
        case 0 :
          break;
        case 1:
          break;
        case 2:
          break;
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      
      appBar: AppBar(
        title: Text("Form.com"),
      ),
      body: Form(
        key: _formKey,
        child: SingleChildScrollView(
          child: Container(
            padding: EdgeInsets.all(20.0),
            child: Column(
              children: [
                Container(
                  alignment: Alignment.center,
                  padding: EdgeInsets.all(15),
                  child: Text(
                    "Sign Up",
                    style: TextStyle(
                      color: Colors.red,
                      fontWeight: FontWeight.w500,
                      fontSize: 30,
                      fontStyle: FontStyle.italic,
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: TextFormField(
                    decoration: new InputDecoration(
                      hintText: "contoh: Susilo Bambang",
                      labelText: "Nama Lengkap",
                      icon: Icon(Icons.people),
                      border: OutlineInputBorder(
                          borderRadius: new BorderRadius.circular(5.0)),
                    ),
                    validator: (value) {
                      if (value.isEmpty) {
                        return 'Nama tidak boleh kosong';
                      }
                      else {
                        name = value;
                      }
                      return null;
                    },
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: TextFormField(
                    decoration: new InputDecoration(
                      hintText: "+62 ",
                      labelText: "Nomor Telepon",
                      icon: Icon(Icons.phone),
                      border: OutlineInputBorder(
                          borderRadius: new BorderRadius.circular(5.0)),
                    ),
                    validator: (value) {
                      if (value.isEmpty) {
                        return 'Nomor Telepon tidak boleh kosong';
                      }
                      return null;
                    },
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: TextFormField(
                    decoration: new InputDecoration(
                      hintText: "contoh: Universitas Indonesia",
                      labelText: "Asal Universitas",
                      icon: Icon(Icons.home_filled),
                      border: OutlineInputBorder(
                          borderRadius: new BorderRadius.circular(5.0)),
                    ),
                    validator: (value) {
                      if (value.isEmpty) {
                        return 'Asal Universitas tidak boleh kosong';
                      }
                      return null;
                    },
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: TextFormField(
                    decoration: new InputDecoration(
                      hintText: "mail@mail.com",
                      labelText: "Email",
                      icon: Icon(Icons.email),
                      border: OutlineInputBorder(
                          borderRadius: new BorderRadius.circular(5.0)),
                    ),
                    validator: (value) {
                      if (value.isEmpty) {
                        return 'Email tidak boleh kosong';
                      }
                      return null;
                    },
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: TextFormField(
                    obscureText: true,
                    decoration: new InputDecoration(
                      labelText: "Password",
                      icon: Icon(Icons.security),
                      border: OutlineInputBorder(
                          borderRadius: new BorderRadius.circular(5.0)),
                    ),
                    validator: (value) {
                      if (value.isEmpty) {
                        return 'Password tidak boleh kosong';
                      }
                      return null;
                    },
                  ),
                ),
                Container(
                  alignment: Alignment.center,
                  padding: EdgeInsets.all(10),
                  child: Text(
                    "Minat Anda",
                    style: TextStyle(
                      fontSize: 20,
                    ),
                  ),
                ),
                CheckboxListTile(
                  title: Text('Data Science'),
                  value: nilaiCheckBox,
                  activeColor: Colors.deepPurpleAccent,
                  onChanged: (value) {
                    setState(() {
                      nilaiCheckBox = value?? true ;
                    });
                  },
                ),
                CheckboxListTile(
                  title: Text('Web Development'),
                  value: nilaiCheckBox1,
                  activeColor: Colors.deepPurpleAccent,
                  onChanged: (value) {
                    setState(() {
                      nilaiCheckBox1 = value?? true ;
                    });
                  },
                ),
                CheckboxListTile(
                  title: Text('Mobile App Development'),
                  value: nilaiCheckBox2,
                  activeColor: Colors.deepPurpleAccent,
                  onChanged: (value) {
                    setState(() {
                      nilaiCheckBox2 = value?? true ;
                    });
                  },
                ),
                Container(
                  alignment: Alignment.center,
                  padding: EdgeInsets.all(10),
                  child: Text(
                    "Gender",
                    style: TextStyle(
                      fontSize: 20,
                    ) ,
                  ),
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    Row(
                      children: [
                        Radio(value: 0, groupValue: radioValue, onChanged: radioValueChange),
                        Text("Male", style:TextStyle(fontSize: 15),),
                        
                      ],
                    ),
                    Row(
                      children: [
                        Radio(value: 1, groupValue: radioValue, onChanged: radioValueChange),
                        Text("Female", style:TextStyle(fontSize: 15),),
                      ],
                    ),           
                  ],
                ),
                SwitchListTile(
                  title: Text('Change Mode'),
                  subtitle: Text('background color change automated'),
                  value: nilaiSwitch,
                  activeTrackColor: Colors.pink[100],
                  activeColor: Colors.red,
                  onChanged: (value) {
                    setState(() {
                      nilaiSwitch = value;
                    });
                  },
                ),
                Container(
                  padding: EdgeInsets.all(10),
                  child: Text("Rate Your Spirit",
                    style: TextStyle(
                      fontSize: 20,
                    ),
                  ),
                ),
                Slider(
                  value: nilaiSlider,
                  min: 0,
                  max: 100,
                  onChanged: (value) {
                    setState(() {
                      nilaiSlider = value;
                    });
                  },
                ),
                RaisedButton(
                  child: Text(
                    "Submit",
                    style: TextStyle(color: Colors.white),
                  ),
                  color: Colors.blue,
                  onPressed: () {
                    
                    if (_formKey.currentState.validate()) {
                      ScaffoldMessenger.of(context).showSnackBar(
                        const SnackBar(content: Text('Processing Data')),
                      );
                      createAlertDialog(name);
                    }
                    
                  },
                ),
              ],
            ),
          ),
        ),
      ),
    backgroundColor: switchBackground(nilaiSwitch),
    );
  }
}