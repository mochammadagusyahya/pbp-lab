## 1. Perbedaan JSON dan XML
JSON dan XML keduanya berfungsi untuk mempertukarkan data dari berbagai sistem web server. Namun, keduanya juga memiliki perbedaan. JSON (JavaScript Object Notation) berbentuk meta-bahasa, sedangkan XML (eXtensible Markup Language) berbentuk bahasa markup yang mirip html.  Lebih jelasnya seperti berikut.
#### JSON : 
-	JSON berorientasi pada data
-	Pemrosesan data lebih cepat
-	JSON menyimpan data dalam format key : value
-	Tidak mendukung namespace, comment dan metadata
-	Keamanan lebih aman
-	File lebih ringkas dan sederhana
#### XML :
-	XML lebih beriorientasi pada dokumen atau teks
-	Pemrosesan data tidak secepat JSON
-	XML menyimpan data sebagai tree structure dengan format tag mirip html namun penamaan tag bebas.
-	Mendukung penggunaan namespace, comment, dan metadata
-	Keamanan lebih rentan terhadap beberapa serangan
-	Ukuran dokumen dan file lebih besar

## 2. Perbedaan HTML dan XML
HTML (Hyper Text Markup Language) merupakan bahasa yang digunakan untuk membuat halaman web dan aplikasi web. XML (eXtensible Markup Language) merupakan bahasa yang berfungsi untuk mengangkut dan menukar data dari berbagi platform. Keduanya sama-sama menggunakan bahasa markup. Namun HTML dan XML punya perbedaan seperti berikut :
#### HTML :
-	Lebih fokus pada penyajian dan tampilan data di halaman web
-	HTML tidak menyediakan dukungan namespace
-	Tag pada HTML terbatas
-	Tag pada HTML sudah memiliki ketentuan sebelumnya
-	Bahasa pada HTML tidak Case Sensitive
#### XML :
-	Lebih fokus pada proses pertukaran atau transfer data antar web.
-	Menyediakan dukungan namespace
-	Tag pada XML masih bisa dikembangkan
-	Tag XML tidak ditentukan seperti HTML
-	Bahasa pada XML bersifat Case Sensitive
